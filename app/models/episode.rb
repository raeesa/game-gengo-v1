class Episode < ApplicationRecord
  belongs_to :chapter
  has_many :words, dependent: :destroy

  def next
    self.class.where("id > ?", id).first
  end

  def previous
    self.class.where("id < ?", id).last
  end
end
