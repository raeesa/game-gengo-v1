class Series < ApplicationRecord
  has_many :chapters, dependent: :destroy
  has_many :episodes, through: :chapters
  has_many :words, through: :episodes
end
