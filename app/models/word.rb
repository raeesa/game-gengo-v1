class Word < ApplicationRecord
  belongs_to :episode

  include PgSearch::Model
  pg_search_scope :word_search,
                  against: %i[word reading definition level],
                  using: {
                    tsearch: { prefix: true }
                  }
end
