class Chapter < ApplicationRecord
  belongs_to :series
  has_many :episodes, dependent: :destroy
  has_many :words, through: :episodes
end
