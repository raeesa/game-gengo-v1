class WordsController < ApplicationController
  def index
    if params[:query].present?
      @words = Word.word_search(params[:query])
    else
      @words = Word.includes(:episode, :chapter, :series)
    end
  end
end
