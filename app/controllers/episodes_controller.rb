class EpisodesController < ApplicationController
  def index
    @episodes = Episode.includes(:chapter)
  end

  def show
    @episode = Episode.find(params[:id])
    @chapter = Chapter.find(params[:chapter_id])
    @series = Series.find(params[:series_id])
    @chapter.series = @series
    @words = Word.all
    @episodes = Episode.all
  end
end
