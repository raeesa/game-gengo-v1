class SeriesController < ApplicationController
  WORDS_PER_PAGE = 50
  def index
    @series = Series.all
  end

  def show
    @series = Series.find(params[:id])
    @chapters = Chapter.all
  end

  def database
    @series = Series.find(params[:id])
    @page = params.fetch(:page, 0).to_i
    @database = @series.words.offset(@page * WORDS_PER_PAGE).limit(WORDS_PER_PAGE)
    @total_series_words = @series.words.size
  end
end
