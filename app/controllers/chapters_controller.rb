class ChaptersController < ApplicationController
  def index
    @chapters = Chapter.includes(:series)
  end

  def show
    @chapter = Chapter.find(params[:id])
    @episodes = Episode.all
    @series = Series.find(params[:series_id])
    @chapter.series = @series
  end
end
