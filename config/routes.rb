Rails.application.routes.draw do
  root to: 'series#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  resources :words
  resources :series do
    member do
      get :database
    end

    resources :chapters do
      resources :episodes
    end
  end
end
