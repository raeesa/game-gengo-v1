class CreateWords < ActiveRecord::Migration[6.0]
  def change
    create_table :words do |t|
      t.string :word
      t.string :reading
      t.string :word_type
      t.string :definition
      t.string :level
      t.references :episode, null: false, foreign_key: true

      t.timestamps
    end
  end
end
