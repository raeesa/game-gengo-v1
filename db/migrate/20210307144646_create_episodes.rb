class CreateEpisodes < ActiveRecord::Migration[6.0]
  def change
    create_table :episodes do |t|
      t.string :name
      t.string :anki_deck
      t.string :youtube_url
      t.string :youtube_id
      t.references :chapter, null: false, foreign_key: true

      t.timestamps
    end
  end
end
