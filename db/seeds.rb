# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Cleaning database..."

Series.destroy_all
Chapter.destroy_all
Episode.destroy_all
Word.destroy_all

# series = Series.create(attributes {})
# chapter = series.chapter.create(attributes{})


ff7r = Series.create!(name: "Final Fantasy VII Remake")

chapter_1 = ff7r.chapters.create!(name: "Chapter 1", series_id: ff7r.id, image_url: "'https://cdnen.samurai-gamers.com/wp-content/uploads/2020/03/04104334/Final-Fantasy-7-Remake-Chapter-1-Walkthrough-1.jpg'")
chapter_2 = ff7r.chapters.create!(name: "Chapter 2", series_id: ff7r.id, image_url: "https://oyster.ignimgs.com/mediawiki/apis.ign.com/final-fantasy-vii-remake/d/df/Final_Fantasy_VII_Remake_-_Chapter_2_6.png?width=1280")
ff7r.chapters.create!(name: "Chapter 3", series_id: ff7r.id, image_url: "https://img.game8.co/3240550/37e1c1ebcfd7ec265ac1e45a488b6861.png/show")
ff7r.chapters.create!(name: "Chapter 4", series_id: ff7r.id, image_url: "https://www.gamerevolution.com/assets/uploads/2020/04/Final-Fantasy-7-remake-biker-boy-trophy-jesse-praise-1280x720.jpg")

puts "Creating episodes..."

Episode.create!(name: "Episode 1", chapter_id: chapter_1.id, youtube_id: "GB1BkptBr9w")
Episode.create!(name: "Episode 2", chapter_id: chapter_1.id, youtube_id: "qs2ngyJ3y48")
Episode.create!(name: "Episode 3", chapter_id: chapter_1.id, youtube_id: "LYP8t9_O-AM")
Episode.create!(name: "Episode 4", chapter_id: chapter_1.id, youtube_id: "IDY9JakQYHo")
Episode.create!(name: "Episode 5", chapter_id: chapter_1.id, youtube_id: "BbTaP9ty0Yk")
Episode.create!(name: "Episode 6", chapter_id: chapter_1.id, youtube_id: "Z8ScIyTrMY4")
Episode.create!(name: "Episode 7", chapter_id: chapter_1.id, youtube_id: "GuiPFDHXX_M")
Episode.create!(name: "Episode 8", chapter_id: chapter_1.id, youtube_id: "WmlzTg8kF9I")
Episode.create!(name: "Episode 9", chapter_id: chapter_1.id, youtube_id: "UW0fyG1o_Oc")
Episode.create!(name: "Episode 10", chapter_id: chapter_1.id, youtube_id: "peLGak1m-T8")
Episode.create!(name: "Episode 11", chapter_id: chapter_2.id, youtube_id: "5YA-bHjCmhc")
Episode.create!(name: "Episode 12", chapter_id: chapter_2.id, youtube_id: "GI0YrbYyWcw")
Episode.create!(name: "Episode 13", chapter_id: chapter_2.id, youtube_id: "eisgDzcAIW8")
Episode.create!(name: "Episode 14", chapter_id: chapter_2.id, youtube_id: "gkT2LUYJXLE")

puts "Creating words..."

require "csv"
csv_text = File.read(Rails.root.join("lib", "csvs", "kotoba.csv"))
csv = CSV.parse(csv_text, :headers => true, :encoding => "UTF-8")
csv.each do |row|
  w = Word.new
  w.word = row["Word"]
  w.reading = row["Reading"]
  w.word_type = row["Word Type"]
  w.definition = row["Defintion"]
  w.level = row["JLPT Level"]
  episode_number = row["Episode"].to_i
  episode = Episode.find(episode_number)
  w.episode_id = episode.id
  w.save
  puts "#{w.word} saved"
end
puts "There are now #{Word.count} rows in the words table"
